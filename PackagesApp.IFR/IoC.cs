﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace PackagesApp.IFR
{
    /// <summary>
    /// Dependency injection class
    /// </summary>
    public class IoC : IDisposable
    {
        protected IUnityContainer container;

        #region Singleton
        private static readonly IoC current = new IoC();

        public static IoC Current
        {
            get
            {
                return current;
            }
        }
        #endregion

        #region metodos publicos
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return container.Resolve<T>(name);
        }

        public object Resolve(Type t)
        {
            return container.Resolve(t);
        }

        public object Resolve(Type t, string name)
        {
            return container.Resolve(t, name);
        }

        public object ResolveAll<T>()
        {
            return container.ResolveAll<T>();
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            return container.ResolveAll(type);
        }
        #endregion

        #region Constructores
        static IoC()
        { }

        protected IoC()
        {
            container = new UnityContainer();
            container.RegisterType(
                GetType("PackagesApp.CORE.Contracts.IApplicationDbContext, PackagesApp.CORE"),
                GetType("PackagesApp.DAL.ApplicationDbContext, PackagesApp.DAL"),
                new ContainerControlledLifetimeManager());
            container.RegisterType(
                GetType("PackagesApp.CORE.Contracts.IDestinationsManager, PackagesApp.CORE"),
                GetType("PackagesApp.Application.Managers.DestinationsManager, PackagesApp.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("PackagesApp.CORE.Contracts.IPackagesManager, PackagesApp.CORE"),
                GetType("PackagesApp.Application.Managers.PackagesManager, PackagesApp.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("PackagesApp.CORE.Contracts.IPackageStatusesManager, PackagesApp.CORE"),
                GetType("PackagesApp.Application.Managers.PackageStatusesManager, PackagesApp.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("PackagesApp.CORE.Contracts.IStatusesManager, PackagesApp.CORE"),
                GetType("PackagesApp.Application.Managers.StatusesManager, PackagesApp.Application"),
                new TransientLifetimeManager());
        }

        private Type GetType(string typeString)
        {
            Type type = Type.GetType(typeString);

            if (type == null)
            {
                throw new Exception("Type can´t be resolved: " + typeString);
            }

            return type;
        }

        public void Dispose()
        {
            container.Dispose();
        }
        #endregion
    }
}