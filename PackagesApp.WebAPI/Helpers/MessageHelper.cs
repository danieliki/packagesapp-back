﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace PackagesApp.WebAPI.Helpers
{
    public static class MessageHelper
    {
        public static List<string> GetModelErrors(ICollection<ModelState> modelStateValues)
        {
            var modelErrors = new List<string>();

            foreach (var error in modelStateValues.SelectMany(v => v.Errors))
            {
                modelErrors.Add(error.ErrorMessage);
            }

            return modelErrors;
        }

        public static IEnumerable<string> GetMessage(string message)
        {
            return new List<string>()
            {
                message
            };
        }
    }
}