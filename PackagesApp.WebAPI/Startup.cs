﻿using System.Diagnostics;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(PackagesApp.WebAPI.Startup))]
namespace PackagesApp.WebAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            Trace.TraceInformation("Starting up....");

            var enableCors = true;
            var corsAllowedDomains = "http://localhost:4200";

            GlobalConfiguration.Configure(c => WebApiConfig.Register(c, enableCors, corsAllowedDomains));

            Trace.TraceInformation("Configuring Swagger");
            GlobalConfiguration.Configure(SwaggerConfig.Register);
            Trace.TraceInformation("Swagger configured");

            Trace.TraceInformation("Using Web API....");
            appBuilder.UseWebApi(GlobalConfiguration.Configuration);

            Trace.TraceInformation("Enabling CORS in Web API....");
            appBuilder.UseCors(CorsOptions.AllowAll);

            GlobalConfiguration.Configuration.EnsureInitialized();
        }
    }
}