using System;
using System.Web.Http;
using Swashbuckle.Application;

namespace PackagesApp.WebAPI
{
    public static class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger("docs/{apiVersion}/swagger", c =>
                {
                    c.SingleApiVersion("v1", "PackagesApp API")
                        .Description("This API contains information about documentation.")
                        .Contact(cc => cc.Name("Yo"));

                    c.IncludeXmlComments(string.Format("{0}/App_Data/PackagesApp.xml", AppDomain.CurrentDomain.BaseDirectory));
                })
                .EnableSwaggerUi(c =>
                {
                    c.DisableValidator();
                    c.InjectStylesheet(thisAssembly, "PackagesApp.WebAPI.Content.Swagger.css");
                });
        }
    }
}
