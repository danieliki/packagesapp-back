﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace PackagesApp.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config, bool enableCors, string corsAllowedDomains)
        {
            //WebAPI routes
            #region routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}", new { id = RouteParameter.Optional });
            #endregion


            //Dependency injection
            config.DependencyResolver = new UnityDependencyResolver();

            //CORS
            if (enableCors)
            {
                var corsAttr = new EnableCorsAttribute(origins: corsAllowedDomains,
                    headers: "*",
                    methods: "GET, POST, OPTIONS, PUT, DELETE")
                {
                    SupportsCredentials = false
                };

                config.EnableCors(corsAttr);
            }

            config.Formatters.JsonFormatter
            .SerializerSettings
            .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}
