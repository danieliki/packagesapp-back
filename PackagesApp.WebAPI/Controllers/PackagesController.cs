﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Web.Http;
using Newtonsoft.Json;
using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;
using PackagesApp.WebAPI.App_GlobalResources;
using PackagesApp.WebAPI.Helpers;

namespace PackagesApp.WebAPI.Controllers
{
    [RoutePrefix("api/packages")]
    public class PackagesController : ApiController
    {
        private readonly IPackagesManager _packageManager;
        private readonly IPackageStatusesManager _packageStatusManager;

        public PackagesController(IPackagesManager packageManager, IPackageStatusesManager packageStatusManager)
        {
            _packageManager = packageManager;
            _packageStatusManager = packageStatusManager;
        }

        /// <summary>Get Package by Qr</summary>
        /// <remarks>Get Package by Qr</remarks>
        /// <returns> Package</returns>
        /// <param name="qrCode"></param>
        /// <response code="200">OK: Package obtained correctly</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to obtain the Package</response>
        [HttpGet]
        [Route("byQrCode/{qrCode:int}")]
        public IHttpActionResult GetByQrCode(int qrCode)
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            try
            {
                var package = _packageManager.Get(qrCode);
                return Content(HttpStatusCode.OK, package);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, PackageResources.msg_error_performing_action);
            }
        }

        /// <summary>Get All Packages in the system</summary>
        /// <remarks>Get All Packages in the system</remarks>
        /// <returns> List of Packages</returns>
        /// <response code="200">OK: Packages obtained correctly</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to obtain the Packages</response>
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            try
            {
                var packages = _packageManager.GetAll();
                return Content(HttpStatusCode.OK, packages);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, PackageResources.msg_error_performing_action);
            }
        }

        /// <summary>Insert Package</summary>
        /// <param name="Package"></param>
        /// <remarks>Insert Package</remarks>
        /// <returns> Messagge.</returns>
        /// <response code="200">OK: Package inserted correctly.</response>
        /// <response code="400">BadRequest: Error messages.</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to insert the Package</response>
        [HttpPost]
        public IHttpActionResult Post(Package package)
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            if (!ModelState.IsValid)
            {
                var modelErrorMessages = MessageHelper.GetModelErrors(ModelState.Values);

                Trace.TraceError(string.Format("[{0}][{1}] Model error: {2}", GetType().Name, MethodBase.GetCurrentMethod().Name, JsonConvert.SerializeObject(ModelState)));
                return Content(HttpStatusCode.BadRequest, modelErrorMessages);
            }

            try
            {
                _packageManager.Insert(package);
                return Content(HttpStatusCode.OK, PackageResources.msg_package_saved_correctly);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, PackageResources.msg_error_performing_action);
            }
        }

        /// <summary>Insert PackageStatus.</summary>
        /// <remarks>Insert PackageStatus.</remarks>
        /// <returns> Messagge.</returns>
        /// <param name="PackageStatus"></param>
        /// <response code="200">OK: Package status inserted correctly.</response>
        /// <response code="400">BadRequest: Error messages.</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to insert the PackageStatus
        /// </response>
        [HttpPost]
        [Route("packageStatus")]
        public IHttpActionResult Post(PackageStatus packageStatus)
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            if (!ModelState.IsValid)
            {
                var modelErrorMessages = MessageHelper.GetModelErrors(ModelState.Values);

                Trace.TraceError(string.Format("[{0}][{1}] Model error: {2}", GetType().Name, MethodBase.GetCurrentMethod().Name, JsonConvert.SerializeObject(ModelState)));
                return Content(HttpStatusCode.BadRequest, modelErrorMessages);
            }

            try
            {
                _packageStatusManager.Insert(packageStatus);
                return Content(HttpStatusCode.OK, PackageResources.msg_package_status_saved_correctly);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, PackageResources.msg_error_performing_action);
            }
        }
    }
}
