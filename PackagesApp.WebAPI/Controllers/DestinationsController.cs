﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Web.Http;
using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;
using PackagesApp.WebAPI.App_GlobalResources;
using PackagesApp.WebAPI.Helpers;

namespace PackagesApp.WebAPI.Controllers
{
    [RoutePrefix("api/destinations")]
    public class DestinationsController : ApiController
    {
        private readonly IManager<Destination> _destinationManager;

        public DestinationsController(IDestinationsManager destinationManager)
        {
            _destinationManager = destinationManager;
        }

        /// <summary>Get all Destinations</summary>
        /// <remarks>Get all Destinations</remarks>
        /// <returns> List of Destinations</returns>
        /// <response code="200">OK: Destinations obtained correctly</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to obtain the Destinations<response>
        [HttpGet]
        public IHttpActionResult Get()
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            try
            {
                var destinations = _destinationManager.GetAll();

                return Content(HttpStatusCode.OK, destinations);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, MessageHelper.GetMessage(StatusResources.msg_error_performing_action));
            }
        }
    }
}
