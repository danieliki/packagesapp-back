﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Web.Http;
using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;
using PackagesApp.WebAPI.App_GlobalResources;

namespace PackagesApp.WebAPI.Controllers
{
    [RoutePrefix("api/statuses")]
    public class StatusesController : ApiController
    {
        private readonly IManager<Status> _statusManager;

        public StatusesController(IStatusesManager statusManager)
        {
            _statusManager = statusManager;
        }

        /// <summary>Get all Statuses</summary>
        /// <remarks>Get all Statuses</remarks>
        /// <returns> List of Statuses</returns>
        /// <response code="200">OK: Statuses obtained correctly</response>
        /// <response code="500">Internal Server Error: An error has occurred trying to obtain the Statuses</response>
        [HttpGet]
        public IHttpActionResult Get()
        {
            Trace.TraceInformation("[{0}][{1}] Begin action.", GetType().Name, MethodBase.GetCurrentMethod().Name);

            try
            {
                var statuses = _statusManager.GetAll();
                return Content(HttpStatusCode.OK, statuses);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("[{0}][{1}] Ex: {2} ", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                return Content(HttpStatusCode.InternalServerError, StatusResources.msg_error_performing_action);
            }
        }
    }
}
