﻿using System;
using System.Collections.Generic;

namespace PackagesApp.CORE.Domain.Classes
{
    /// <summary>
    /// Package class 
    /// </summary>
    public class Package
    {
        public int Id { get; set; }

        public int QrCode { get; set; }

        public int ShippingNumber { get; set; }

        public DateTime ShippingDate { get; set; }

        public string Dimensions { get; set; }

        public int Weight { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int DestinationId { get; set; }

        public Destination Destination { get; set; }

        public ICollection<PackageStatus> PackageStatuses { get; set; }
    }
}
