﻿namespace PackagesApp.CORE.Domain.Classes
{
    /// <summary>
    /// Destination class
    /// </summary>
    public class Destination
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Package Package { get; set; }
    }
}
