﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PackagesApp.CORE.Domain.Classes
{
    /// <summary>
    /// Package status class
    /// </summary>
    public class PackageStatus
    {
        [Key]
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int PackageId { get; set; }

        public Package Package { get; set; }

        public int StatusId { get; set; }

        public Status Status { get; set; }
    }
}

