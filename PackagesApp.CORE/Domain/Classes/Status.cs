﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PackagesApp.CORE.Domain.Classes
{
    /// <summary>
    /// Status class
    /// </summary>
    public class Status
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<PackageStatus> PackageStatuses { get; set; }
    }
}
