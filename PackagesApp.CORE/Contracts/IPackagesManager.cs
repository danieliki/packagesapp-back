﻿using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.CORE.Contracts
{
    public interface IPackagesManager : IManager<Package>
    {
        Package Get(int qrCode);
        void Insert(Package package);
    }
}
