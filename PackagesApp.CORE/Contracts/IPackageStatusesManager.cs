﻿using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.CORE.Contracts
{
    public interface IPackageStatusesManager : IManager<PackageStatus>
    {
        void Insert(PackageStatus packageStatus);
    }
}
