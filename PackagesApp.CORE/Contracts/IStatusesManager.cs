﻿using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.CORE.Contracts
{
    public interface IStatusesManager : IManager<Status>
    {
    }
}
