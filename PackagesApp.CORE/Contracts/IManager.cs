﻿using System.Collections.Generic;

namespace PackagesApp.CORE.Contracts
{
    /// <summary>
    /// Generic methods interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IManager<T>
        where T : class
    {
        IApplicationDbContext db { get; }

        IEnumerable<T> GetAll(string include = null);

        T Add(T entity);

        T Delete(T entity);

        int SaveChanges();
    }
}
