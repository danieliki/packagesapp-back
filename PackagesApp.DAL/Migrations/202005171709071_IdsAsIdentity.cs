﻿namespace PackagesApp.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdsAsIdentity : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Destination", new[] { "Id" });
            DropPrimaryKey("dbo.Destination");
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Destination", "Id");
            CreateIndex("dbo.Destination", "Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Destination", new[] { "Id" });
            DropPrimaryKey("dbo.Destination");
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Destination", "Id");
            CreateIndex("dbo.Destination", "Id");
        }
    }
}
