﻿namespace PackagesApp.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class First : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Destination");
            AddColumn("dbo.Package", "Dimensions", c => c.Int(nullable: false));
            AddColumn("dbo.Package", "Weight", c => c.Int(nullable: false));
            AddColumn("dbo.Package", "DestinationId", c => c.Int(nullable: false));
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Destination", "Id");
            CreateIndex("dbo.Destination", "Id");
            AddForeignKey("dbo.Destination", "Id", "dbo.Package", "Id");
            DropColumn("dbo.Package", "Quantity");
            DropColumn("dbo.Package", "Kgs");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.PackageDestination",
                c => new
                {
                    PackageDestinationId = c.Int(nullable: false),
                    CreatedBy = c.String(),
                    CreatedOn = c.DateTime(nullable: false),
                    Id = c.Int(nullable: false),
                    DestinationId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.PackageDestinationId);

            AddColumn("dbo.Package", "Kgs", c => c.Int());
            AddColumn("dbo.Package", "Quantity", c => c.Int());
            DropForeignKey("dbo.Destination", "Id", "dbo.Package");
            DropIndex("dbo.Destination", new[] { "Id" });
            DropPrimaryKey("dbo.Destination");
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Package", "DestinationId");
            DropColumn("dbo.Package", "Weight");
            DropColumn("dbo.Package", "Dimensions");
            AddPrimaryKey("dbo.Destination", "Id");
            CreateIndex("dbo.PackageDestination", "DestinationId");
            CreateIndex("dbo.PackageDestination", "PackageDestinationId");
            AddForeignKey("dbo.PackageDestination", "PackageDestinationId", "dbo.Package", "Id");
            AddForeignKey("dbo.PackageDestination", "DestinationId", "dbo.Destination", "Id", cascadeDelete: true);
        }
    }
}
