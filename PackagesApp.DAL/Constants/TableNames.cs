﻿namespace PackagesApp.DAL.Constants
{
    public static class TableNames
    {
        public const string Package = "Package";
        public const string Status = "Status";
        public const string PackageStatus = "PackageStatus";
        public const string Destination = "Destination";
        public const string PackageDestination = "PackageDestination";
    }
}
