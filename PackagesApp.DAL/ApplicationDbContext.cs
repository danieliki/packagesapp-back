﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;
using PackagesApp.DAL.Constants;

namespace PackagesApp.DAL
{
    /// <summary>
    /// App data context class
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<ApplicationDbContext>(null);
            Database.CreateIfNotExists();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasKey(r => r.Id).Property(p => p.Name).IsRequired();
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });

            modelBuilder.Entity<Package>().HasKey(p => p.Id).ToTable(TableNames.Package, schemaName: SchemaNames.dbo);
            modelBuilder.Entity<PackageStatus>().HasKey(p => p.Id).ToTable(TableNames.PackageStatus, schemaName: SchemaNames.dbo);
            modelBuilder.Entity<Status>().HasKey(p => p.Id).ToTable(TableNames.Status, schemaName: SchemaNames.dbo);
            modelBuilder.Entity<Destination>().HasKey(p => p.Id).ToTable(TableNames.Destination, schemaName: SchemaNames.dbo);

            modelBuilder.Entity<Package>().Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PackageStatus>().Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Status>().Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Destination>().Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //modelBuilder.Entity<Package>()
            //    .HasMany(m => m.PackageStatuses)
            //    .WithRequired(c => c.Package)
            //    .HasForeignKey(m => m.PackageId);

            modelBuilder.Entity<PackageStatus>()
                .HasRequired(c => c.Package)
                .WithMany(g => g.PackageStatuses)
                .HasForeignKey(m => m.PackageId);

            modelBuilder.Entity<Package>()
                .HasRequired(s => s.Destination)
                .WithRequiredPrincipal(ad => ad.Package);

            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Statuses persistent data collection
        /// </summary>
        public DbSet<Status> Statuses { get; set; }

        /// <summary>
        /// Destinations persistent data collection
        /// </summary>
        public DbSet<Destination> Destinations { get; set; }

        /// <summary>
        /// Packages persistent data collection
        /// </summary>
        public DbSet<Package> Packages { get; set; }

        /// <summary>
        /// PackageStatuses persistent data collection
        /// </summary>
        public DbSet<PackageStatus> PackageStatuses { get; set; }
    }
}
