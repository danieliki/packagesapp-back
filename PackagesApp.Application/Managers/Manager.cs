﻿using System.Collections.Generic;
using PackagesApp.CORE.Contracts;

namespace PackagesApp.Application.Managers
{
    /// <summary>
    /// Generic Manager
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Manager<T> : IManager<T>
        where T : class
    {
        public IApplicationDbContext db { get; }

        /// <summary>
        /// Generic manager constructor
        /// </summary>
        /// <param name="db"></param>
        public Manager(IApplicationDbContext _db)
        {
            db = _db;
        }

        /// <summary>
        /// Get all class db registers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll(string include = null)
        {
            if (include == null)
            {
                return db.Set<T>();
            }
            return db.Set<T>().Include(include);
        }

        /// <summary>
        /// Adds a DB register
        /// </summary>
        /// <param name="entity"></param>
        public T Add(T entity)
        {
            return db.Set<T>().Add(entity);
        }

        /// <summary>
        /// Deletes a DB register
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Delete(T entity)
        {
            return db.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Persist data
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return db.SaveChanges();
        }
    }
}
