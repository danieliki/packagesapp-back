﻿using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.Application.Managers
{
    public class DestinationsManager : Manager<Destination>, IDestinationsManager
    {
        public DestinationsManager(IApplicationDbContext _db) : base(_db)
        { }
    }
}
