﻿using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.Application.Managers
{
    public class StatusesManager : Manager<Status>, IStatusesManager
    {
        #region .: Private Variables :.



        #endregion .: Private Variables :.

        #region .: Constructor :.
        public StatusesManager(IApplicationDbContext _db) : base(_db)
        { }
        #endregion .: Constructor :.

        #region .: Public Methods :.



        #endregion .: Public Methods :.
    }
}
