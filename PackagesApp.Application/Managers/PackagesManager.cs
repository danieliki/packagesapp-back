﻿using System.Linq;
using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.Application.Managers
{
    public class PackagesManager : Manager<Package>, IPackagesManager
    {
        public PackagesManager(IApplicationDbContext _db) : base(_db)
        { }

        public Package Get(int qrCode)
        {
            return db.Packages.FirstOrDefault(p => p.QrCode == qrCode);
        }

        public void Insert(Package package)
        {
            Add(package);
            db.SaveChanges();
        }
    }
}
