﻿using PackagesApp.CORE.Contracts;
using PackagesApp.CORE.Domain.Classes;

namespace PackagesApp.Application.Managers
{
    public class PackageStatusesManager : Manager<PackageStatus>, IPackageStatusesManager
    {
        public PackageStatusesManager(IApplicationDbContext _db) : base(_db)
        { }

        public void Insert(PackageStatus packageStatus)
        {
            Add(packageStatus);
            db.SaveChanges();
        }
    }
}
